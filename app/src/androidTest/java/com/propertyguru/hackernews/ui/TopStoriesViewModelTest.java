package com.propertyguru.hackernews.ui;

import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Created by zr.faisal on 5/12/17.
 */

@RunWith(JUnit4.class)
public class TopStoriesViewModelTest {

    /*
    private TopStoriesViewModel viewModel;
    private ApiRepository

    @Before
    public void init() {
        repository = mock(RepoRepository.class);
        viewModel = new TopStoriesViewModel(repository);
    }

    public void empty() {
        Observer<Resource<List<Repo>>> result = mock(Observer.class);
        viewModel.getResults().observeForever(result);
        viewModel.loadNextPage();
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void basic() {
        Observer<Resource<List<Repo>>> result = mock(Observer.class);
        viewModel.getResults().observeForever(result);
        viewModel.setQuery("foo");
        verify(repository).search("foo");
        verify(repository, never()).searchNextPage("foo");
    }

    @Test
    public void noObserverNoQuery() {
        when(repository.searchNextPage("foo")).thenReturn(mock(LiveData.class));
        viewModel.setQuery("foo");
        verify(repository, never()).search("foo");
        // next page is user interaction and even if loading state is not observed, we query
        // would be better to avoid that if main search query is not observed
        viewModel.loadNextPage();
        verify(repository).searchNextPage("foo");
    }

    @Test
    public void swap() {
        LiveData<Resource<Boolean>> nextPage = new MutableLiveData<>();
        when(repository.searchNextPage("foo")).thenReturn(nextPage);

        Observer<Resource<List<Repo>>> result = mock(Observer.class);
        viewModel.getResults().observeForever(result);
        verifyNoMoreInteractions(repository);
        viewModel.setQuery("foo");
        verify(repository).search("foo");
        viewModel.loadNextPage();

        viewModel.getLoadMoreStatus().observeForever(mock(Observer.class));
        verify(repository).searchNextPage("foo");
        assertThat(nextPage.hasActiveObservers(), is(true));
        viewModel.setQuery("bar");
        assertThat(nextPage.hasActiveObservers(), is(false));
        verify(repository).search("bar");
        verify(repository, never()).searchNextPage("bar");
    }

    @Test
    public void refresh() {
        viewModel.refresh();
        verifyNoMoreInteractions(repository);
        viewModel.setQuery("foo");
        viewModel.refresh();
        verifyNoMoreInteractions(repository);
        viewModel.getResults().observeForever(mock(Observer.class));
        verify(repository).search("foo");
        reset(repository);
        viewModel.refresh();
        verify(repository).search("foo");
    }

    @Test
    public void resetSameQuery() {
        viewModel.getResults().observeForever(mock(Observer.class));
        viewModel.setQuery("foo");
        verify(repository).search("foo");
        reset(repository);
        viewModel.setQuery("FOO");
        verifyNoMoreInteractions(repository);
        viewModel.setQuery("bar");
        verify(repository).search("bar");
    }
    */

}
