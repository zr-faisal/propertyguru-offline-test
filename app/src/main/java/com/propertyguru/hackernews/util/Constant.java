package com.propertyguru.hackernews.util;

/**
 * Created by zr.faisal on 30/11/17.
 */

public class Constant {

    public static final String ENDPOINT = "https://hacker-news.firebaseio.com/v0/";
    public static final String EXTRA_KIDS = "EXTRA_KIDS";
    public static final int TIMEOUT_IN_SEC = 15;

}
