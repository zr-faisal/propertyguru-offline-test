package com.propertyguru.hackernews.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by zr.faisal on 30/11/17.
 */

public class Item {

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("text")
    public String text;

    @SerializedName("type")
    public String type;

    @SerializedName("by")
    public String by;

    @SerializedName("time")
    public long time;

    @SerializedName("url")
    public String url;

    @SerializedName("score")
    public int score;

    @SerializedName("descendants")
    public int descendants;

    @SerializedName("kids")
    public List<Integer> kids;

}
