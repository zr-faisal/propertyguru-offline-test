package com.propertyguru.hackernews.api;

import android.support.annotation.NonNull;

import com.propertyguru.hackernews.api.model.Comment;
import com.propertyguru.hackernews.api.model.Item;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by zr.faisal on 30/11/17.
 */

public interface ApiServices {

    @NonNull
    @GET("topstories.json")
    Call<List<Integer>> loadTopStories();

    @NonNull
    @GET("item/{id}.json")
    Call<Item> loadStory(@Path("id") String id);

    @NonNull
    @GET("item/{id}.json")
    Call<Comment> loadComment(@Path("id") String id);

}
