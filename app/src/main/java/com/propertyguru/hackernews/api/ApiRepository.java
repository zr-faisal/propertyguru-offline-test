package com.propertyguru.hackernews.api;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.propertyguru.hackernews.api.model.Comment;
import com.propertyguru.hackernews.api.model.Item;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by zr.faisal on 30/11/17.
 */

public class ApiRepository {

    private final ApiServices apiServices;

    @Inject
    public ApiRepository(ApiServices apiServices) {
        this.apiServices = apiServices;
    }

    @NonNull
    public LiveData<List<Integer>> loadTopStoryIds() {
        final MutableLiveData<List<Integer>> data = new MutableLiveData<>();
        apiServices.loadTopStories().enqueue(new Callback<List<Integer>>() {
            @Override
            public void onResponse(@NonNull Call<List<Integer>> call, @NonNull Response<List<Integer>> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<Integer>> call, @NonNull Throwable t) {

            }
        });
        return data;
    }

    @NonNull
    public LiveData<List<Item>> loadTopStories(@NonNull List<Integer> topStories) {
        final List<Item> items = new ArrayList<>();
        final MutableLiveData<List<Item>> data = new MutableLiveData<>();
        data.setValue(items);

        for (Integer id : topStories) {
            apiServices.loadStory(String.valueOf(id)).enqueue(new Callback<Item>() {
                @Override
                public void onResponse(@NonNull Call<Item> call, @NonNull Response<Item> response) {
                    items.add(response.body());
                    data.setValue(items);
                }

                @Override
                public void onFailure(@NonNull Call<Item> call, @NonNull Throwable t) {

                }
            });
        }
        return data;
    }

    @NonNull
    public LiveData<List<Comment>> loadComments(@NonNull List<Integer> commentIds) {
        final List<Comment> items = new ArrayList<>();
        final MutableLiveData<List<Comment>> data = new MutableLiveData<>();
        data.setValue(null);

        for (Integer id : commentIds) {
            apiServices.loadComment(String.valueOf(id)).enqueue(new Callback<Comment>() {
                @Override
                public void onResponse(@NonNull Call<Comment> call, @NonNull Response<Comment> response) {
                    items.add(response.body());
                    data.setValue(items);
                }

                @Override
                public void onFailure(@NonNull Call<Comment> call, @NonNull Throwable t) {

                }
            });
        }
        return data;
    }

    @NonNull
    public LiveData<List<Comment>> loadChildComments(@NonNull List<Integer> childCommentIds) {
        final List<Comment> items = new ArrayList<>();
        final MediatorLiveData<List<Comment>> data = new MediatorLiveData<>();
        data.setValue(null);

        final int count = childCommentIds.size() - 1;
        for (int i = 0; i < childCommentIds.size(); i++) {
            final int finalI = i;
            apiServices.loadComment(String.valueOf(childCommentIds.get(i))).enqueue(new Callback<Comment>() {
                int index = finalI;

                @Override
                public void onResponse(@NonNull Call<Comment> call, @NonNull Response<Comment> response) {
                    items.add(response.body());
                    // Publish value if all items loaded
                    if (index == count) {
                        data.setValue(items);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Comment> call, @NonNull Throwable t) {

                }
            });
        }
        return data;
    }

}
