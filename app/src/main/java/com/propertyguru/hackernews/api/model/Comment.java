package com.propertyguru.hackernews.api.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zr.faisal on 4/12/17.
 */

public class Comment implements Parcelable {

    public static final Parcelable.Creator<Comment> CREATOR = new Parcelable.Creator<Comment>() {
        @Override
        public Comment createFromParcel(@NonNull Parcel source) {
            return new Comment(source);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };
    @SerializedName("id")
    public int id;
    @SerializedName("text")
    public String text;
    @SerializedName("type")
    public String type;
    @SerializedName("by")
    public String by;
    @SerializedName("time")
    public long time;
    @SerializedName("url")
    public String url;
    @SerializedName("score")
    public int score;
    @SerializedName("parent")
    public int parent;
    @SerializedName("descendants")
    public int descendants;
    @SerializedName("kids")
    public List<Integer> kids;
    public List<Comment> childComments;

    public Comment() {
        childComments = new ArrayList<>();
    }

    protected Comment(@NonNull Parcel in) {
        this.id = in.readInt();
        this.text = in.readString();
        this.type = in.readString();
        this.by = in.readString();
        this.time = in.readLong();
        this.url = in.readString();
        this.score = in.readInt();
        this.parent = in.readInt();
        this.descendants = in.readInt();
        this.kids = new ArrayList<>();
        in.readList(this.kids, Integer.class.getClassLoader());
        this.childComments = new ArrayList<>();
        in.readList(this.childComments, Comment.class.getClassLoader());
    }

    public void addReply(Comment reply) {
        childComments.add(reply);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.text);
        dest.writeString(this.type);
        dest.writeString(this.by);
        dest.writeLong(this.time);
        dest.writeString(this.url);
        dest.writeInt(this.score);
        dest.writeInt(this.parent);
        dest.writeInt(this.descendants);
        dest.writeList(this.kids);
        dest.writeList(this.childComments);
    }

}
