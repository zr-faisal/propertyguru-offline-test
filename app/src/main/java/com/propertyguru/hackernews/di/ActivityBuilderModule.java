package com.propertyguru.hackernews.di;

import android.support.annotation.NonNull;

import com.propertyguru.hackernews.ui.comments.CommentsActivity;
import com.propertyguru.hackernews.ui.main.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by zr.faisal on 30/11/17.
 */

@Module
public abstract class ActivityBuilderModule {

    @NonNull
    @ContributesAndroidInjector(modules = FragmentBuilderModule.class)
    abstract MainActivity mainActivity();

    @NonNull
    @ContributesAndroidInjector(modules = FragmentBuilderModule.class)
    abstract CommentsActivity commentsActivity();

}

