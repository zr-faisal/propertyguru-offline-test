package com.propertyguru.hackernews.di;

import android.app.Application;
import android.support.annotation.NonNull;

import com.propertyguru.hackernews.HackerNewsApp;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

/**
 * Created by zr.faisal on 30/11/17.
 */

@Singleton
@Component(modules = {
        AppModule.class,
        AndroidInjectionModule.class,
        ActivityBuilderModule.class})
public interface AppComponent {

    void inject(HackerNewsApp app);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        @NonNull
        AppComponent build();
    }

}
