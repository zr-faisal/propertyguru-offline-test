package com.propertyguru.hackernews.di;

import android.support.annotation.NonNull;

import com.propertyguru.hackernews.ui.comments.CommentsFragment;
import com.propertyguru.hackernews.ui.main.TopStoriesFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by zr.faisal on 30/11/17.
 */

@Module
public abstract class FragmentBuilderModule {

    @NonNull
    @ContributesAndroidInjector
    abstract TopStoriesFragment contributeTopStoriesFragment();

    @NonNull
    @ContributesAndroidInjector
    abstract CommentsFragment contributeCommentsFragment();


}
