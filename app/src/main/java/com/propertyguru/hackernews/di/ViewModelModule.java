package com.propertyguru.hackernews.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.propertyguru.hackernews.ui.comments.CommentsViewModel;
import com.propertyguru.hackernews.ui.main.TopStoriesViewModel;
import com.propertyguru.hackernews.util.ViewModelFactory;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created by zr.faisal on 30/11/17.
 */

@Module
public abstract class ViewModelModule {

    @NonNull
    @Binds
    @IntoMap
    @ViewModelKey(TopStoriesViewModel.class)
    abstract ViewModel bindsTopStoriesViewModel(TopStoriesViewModel topStoriesViewModel);

    @NonNull
    @Binds
    @IntoMap
    @ViewModelKey(CommentsViewModel.class)
    abstract ViewModel bindsCommentsViewModel(CommentsViewModel commentsViewModel);


    @NonNull
    @Binds
    abstract ViewModelProvider.Factory bindsViewModelFactory(ViewModelFactory viewModelFactory);
}
