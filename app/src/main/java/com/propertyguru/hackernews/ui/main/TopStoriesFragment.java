package com.propertyguru.hackernews.ui.main;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.propertyguru.hackernews.R;
import com.propertyguru.hackernews.api.model.Item;
import com.propertyguru.hackernews.ui.comments.CommentsActivity;
import com.propertyguru.hackernews.util.Constant;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TopStoriesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TopStoriesFragment extends Fragment
        implements TopStoriesAdapter.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    TopStoriesViewModel topStoriesViewModel;

    @Nullable
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Nullable
    @BindView(R.id.recyclerViewTopStories)
    RecyclerView recyclerViewTopStories;

    @Nullable
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;

    private TopStoriesAdapter adapter;

    public TopStoriesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TopStoriesFragment.
     */
    @NonNull
    public static TopStoriesFragment newInstance() {
        TopStoriesFragment fragment = new TopStoriesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidSupportInjection.inject(this);
        setRetainInstance(true);

        // Initialize adapter
        adapter = new TopStoriesAdapter(new ArrayList<Item>(), TopStoriesFragment.this);
        // Load top story ids
        fetchAndObserveTopStoryIds();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_list, container, false);
        ButterKnife.bind(this, view);

        assert recyclerViewTopStories != null;
        recyclerViewTopStories.setAdapter(adapter);
        assert swipeRefresh != null;
        swipeRefresh.setOnRefreshListener(this);
        setLoadingIndicator(true);

        return view;
    }

    private void fetchAndObserveTopStoryIds() {
        topStoriesViewModel.loadTopStoryIds();
        if (topStoriesViewModel.getTopStoryIds() != null) {
            topStoriesViewModel.getTopStoryIds().observe(this, new Observer<List<Integer>>() {
                @Override
                public void onChanged(@Nullable List<Integer> topStoryIds) {
                    if (topStoryIds != null && !topStoryIds.isEmpty()) {
                        observeTopStories(topStoryIds);
                        setLoadingIndicator(false);
                    }
                }
            });
        }
    }

    private void observeTopStories(final List<Integer> topStoryIds) {
        topStoriesViewModel.loadStories(topStoryIds);
        if (topStoriesViewModel.getTopStories() != null) {
            topStoriesViewModel.getTopStories().observe(this, new Observer<List<Item>>() {
                @Override
                public void onChanged(@Nullable List<Item> items) {
                    if (items != null && !items.isEmpty()) {
                        adapter.addNewItems(items);
                    }
                    setLoadingIndicator(false);
                    assert swipeRefresh != null;
                    if (swipeRefresh.isRefreshing()) {
                        swipeRefresh.setRefreshing(false);
                    }
                }
            });
            setLoadingIndicator(true);
        }
    }

    private void setLoadingIndicator(boolean show) {
        assert progressBar != null;
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onItemClick(@NonNull Item item, View view) {
        if (item.kids != null && !item.kids.isEmpty()) {
            Intent commentsIntent = new Intent(getActivity(), CommentsActivity.class);
            commentsIntent.putIntegerArrayListExtra(Constant.EXTRA_KIDS, new ArrayList<>(item.kids));
            startActivity(commentsIntent);
        } else {
            Toast.makeText(getContext(), getString(R.string.no_comments), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRefresh() {
        assert swipeRefresh != null;
        swipeRefresh.setRefreshing(true);
        fetchAndObserveTopStoryIds();
    }

}
