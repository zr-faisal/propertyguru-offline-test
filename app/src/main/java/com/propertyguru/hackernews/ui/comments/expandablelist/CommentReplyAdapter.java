package com.propertyguru.hackernews.ui.comments.expandablelist;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.propertyguru.hackernews.R;
import com.propertyguru.hackernews.api.model.Comment;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by zr.faisal on 5/12/17.
 */

public class CommentReplyAdapter extends ExpandableRecyclerViewAdapter<CommentViewHolder, ReplyViewHolder> {

    public List<CommentReply> groups;

    public CommentReplyAdapter(@NonNull List<CommentReply> groups) {
        super(groups);
        this.groups = groups;
    }

    @NonNull
    @Override
    public CommentViewHolder onCreateGroupViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_comments_list, parent, false);
        return new CommentViewHolder(view);
    }

    @Override
    public void onBindGroupViewHolder(@NonNull CommentViewHolder holder, int flatPosition, @NonNull ExpandableGroup group) {
        final CommentReply comment = (CommentReply) group;
        holder.bind(group, comment.postedBy, comment.time);
    }

    @NonNull
    @Override
    public ReplyViewHolder onCreateChildViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_replies_list, parent, false);
        return new ReplyViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(@NonNull ReplyViewHolder holder, int flatPosition, @NonNull ExpandableGroup group, int childIndex) {
        final Comment commentReply = ((CommentReply) group).getItems().get(childIndex);
        holder.bind(commentReply.text, commentReply.by, commentReply.time);
    }

}
