package com.propertyguru.hackernews.ui.comments.expandablelist;

import com.propertyguru.hackernews.api.model.Comment;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by zr.faisal on 5/12/17.
 */

public class CommentReply extends ExpandableGroup<Comment> {

    public String title;
    public String postedBy;
    public long time;
    public List<Comment> replies;

    public CommentReply(String title, String postedBy, long time, List<Comment> replies) {
        super(title, replies);
        this.postedBy = postedBy;
        this.time = time;
        this.replies = replies;
    }

}
