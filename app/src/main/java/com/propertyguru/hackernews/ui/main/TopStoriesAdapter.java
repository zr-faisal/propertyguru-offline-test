package com.propertyguru.hackernews.ui.main;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.propertyguru.hackernews.R;
import com.propertyguru.hackernews.api.model.Item;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by zr.faisal on 2/12/17.
 */

public class TopStoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<Item> items;
    private final TopStoriesAdapter.OnItemClickListener itemClickListener;

    public TopStoriesAdapter(List<Item> products, OnItemClickListener itemClickListener) {
        this.items = products;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_topstory_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final Item item = items.get(position);
        ((ViewHolder) holder).bind(item, itemClickListener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addNewItems(@NonNull List<Item> newList) {
        Item lastItem = newList.get(newList.size() - 1);
        items.add(lastItem);
        notifyItemInserted(items.size() - 1);
    }

    /**
     * Listener interface for item-click
     */
    interface OnItemClickListener {
        void onItemClick(Item item, View view);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @BindView(R.id.textViewTitle)
        TextView textViewTitle;

        @Nullable
        @BindView(R.id.textViewSubtitle)
        TextView textViewSubtitle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        /**
         * Binds Item info with ViewHolder views and sets click listener
         *
         * @param item
         * @param itemClickListener
         */
        public void bind(@NonNull final Item item, @Nullable final OnItemClickListener itemClickListener) {
            if (!TextUtils.isEmpty(item.title)) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    assert textViewTitle != null;
                    textViewTitle.setText(Html.fromHtml(item.title, Html.FROM_HTML_MODE_COMPACT));
                } else {
                    assert textViewTitle != null;
                    textViewTitle.setText(Html.fromHtml(item.title));
                }
            }
            assert textViewSubtitle != null;
            textViewSubtitle.setText(textViewSubtitle.getContext()
                    .getString(R.string.subtitle_text, item.score, item.by,
                            item.kids == null ? item.descendants : item.kids.size()));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.onItemClick(item, v);
                    }
                }
            });
        }

    }

}
