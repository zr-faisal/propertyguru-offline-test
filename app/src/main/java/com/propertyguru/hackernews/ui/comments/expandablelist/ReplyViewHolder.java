package com.propertyguru.hackernews.ui.comments.expandablelist;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.TextView;

import com.propertyguru.hackernews.R;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by zr.faisal on 5/12/17.
 */

public class ReplyViewHolder extends ChildViewHolder {

    @Nullable
    @BindView(R.id.textViewTime)
    TextView textViewTime;

    @Nullable
    @BindView(R.id.textViewTitle)
    TextView textViewTitle;

    public ReplyViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(String text, String postedBy, long time) {
        assert textViewTime != null;
        textViewTime.setText(TextUtils.isEmpty(postedBy) ? "" : postedBy + " "
                + DateUtils.getRelativeTimeSpanString(time * 1000,
                System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS));
        if (!TextUtils.isEmpty(text)) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                assert textViewTitle != null;
                textViewTitle.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT));
            } else {
                assert textViewTitle != null;
                textViewTitle.setText(Html.fromHtml(text));
            }
        }
    }

}
