package com.propertyguru.hackernews.ui.comments;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.propertyguru.hackernews.R;
import com.propertyguru.hackernews.api.model.Comment;
import com.propertyguru.hackernews.ui.comments.expandablelist.CommentReply;
import com.propertyguru.hackernews.ui.comments.expandablelist.CommentReplyAdapter;
import com.propertyguru.hackernews.util.Constant;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;

/**
 * Created by zr.faisal on 3/12/17.
 */

public class CommentsFragment extends Fragment {

    @Inject
    CommentsViewModel commentsViewModel;

    @Nullable
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Nullable
    @BindView(R.id.recyclerViewComments)
    RecyclerView recyclerViewComments;

    private List<Comment> commentList;

    public CommentsFragment() {
        // Required empty public constructor
    }

    @NonNull
    public static CommentsFragment newInstance(ArrayList<Integer> commentIds) {
        CommentsFragment fragment = new CommentsFragment();

        Bundle arguments = new Bundle();
        arguments.putIntegerArrayList(Constant.EXTRA_KIDS, commentIds);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidSupportInjection.inject(this);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comments, container, false);
        ButterKnife.bind(this, view);

        ArrayList<Integer> commentIds = getArguments().getIntegerArrayList(Constant.EXTRA_KIDS);
        if (commentIds != null && !commentIds.isEmpty()) {
            observeComments(commentIds);
        }

        return view;
    }

    private void observeComments(final List<Integer> commentIds) {
        commentsViewModel.loadComments(commentIds);
        if (commentsViewModel.getComments() != null) {
            commentsViewModel.getComments().observe(this, new Observer<List<Comment>>() {
                @Override
                public void onChanged(@Nullable List<Comment> comments) {
                    if (comments != null) {
                        commentList = new ArrayList<>();
                        for (Comment comment : comments) {
                            commentList.add(comment);
                            if (comment.kids != null && !comment.kids.isEmpty()) {
                                commentsViewModel.loadReplies(comment.kids);
                            }
                        }
                        observeReplies();
                    }
                }
            });
            setLoadingIndicator(true);
        }
    }

    private void observeReplies() {
        if (commentsViewModel.getReplies() != null) {
            commentsViewModel.getReplies().observe(this, new Observer<List<Comment>>() {
                @Override
                public void onChanged(@Nullable List<Comment> replies) {
                    setLoadingIndicator(false);
                    if (replies != null) {
                        for (Comment reply : replies) {
                            mergeReplyWithParentComment(reply);
                        }
                        setCommentReplyAdapter();
                    }
                }
            });
        }
    }

    private void setLoadingIndicator(boolean show) {
        assert progressBar != null;
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void mergeReplyWithParentComment(@NonNull Comment reply) {
        if (commentList != null) {
            for (Comment parent : commentList) {
                if (parent.id == reply.parent) {
                    parent.addReply(reply);
                }
            }
        }
    }

    private void setCommentReplyAdapter() {
        if (commentList != null && !commentList.isEmpty()) {
            List<CommentReply> commentReplyList = new ArrayList<>();
            for (Comment comment : commentList) {
                CommentReply commentReply = new CommentReply(
                        comment.text, comment.by, comment.time, comment.childComments);
                commentReplyList.add(commentReply);
            }
            assert recyclerViewComments != null;
            recyclerViewComments.setAdapter(new CommentReplyAdapter(commentReplyList));
        }
    }

}
