package com.propertyguru.hackernews.ui.comments.expandablelist;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.propertyguru.hackernews.R;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by zr.faisal on 5/12/17.
 */

public class CommentViewHolder extends GroupViewHolder {

    @Nullable
    @BindView(R.id.textViewTime)
    TextView textViewTime;

    @Nullable
    @BindView(R.id.textViewTitle)
    TextView textViewTitle;

    @Nullable
    @BindView(R.id.imageViewExpand)
    ImageView imageViewExpand;

    public CommentViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(@NonNull ExpandableGroup group, String postedBy, long time) {
        assert textViewTime != null;
        textViewTime.setText(postedBy + " "
                + DateUtils.getRelativeTimeSpanString(time * 1000,
                System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS));
        if (!TextUtils.isEmpty(group.getTitle())) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                assert textViewTitle != null;
                textViewTitle.setText(Html.fromHtml(group.getTitle(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                assert textViewTitle != null;
                textViewTitle.setText(Html.fromHtml(group.getTitle()));
            }
            assert imageViewExpand != null;
            imageViewExpand.setVisibility(group.getItemCount() > 0 ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void expand() {
        assert imageViewExpand != null;
        if (imageViewExpand.isShown()) {
            imageViewExpand.animate().rotationBy(180);
        }
    }

    @Override
    public void collapse() {
        assert imageViewExpand != null;
        if (imageViewExpand.isShown()) {
            imageViewExpand.animate().rotationBy(180);
        }
    }

}
