package com.propertyguru.hackernews.ui.comments;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.propertyguru.hackernews.api.ApiRepository;
import com.propertyguru.hackernews.api.model.Comment;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by zr.faisal on 3/12/17.
 */

public class CommentsViewModel extends ViewModel {

    private ApiRepository apiRepository;
    private LiveData<List<Comment>> comments;
    private LiveData<List<Comment>> replies;

    @Inject
    public CommentsViewModel(ApiRepository apiRepository) {
        this.apiRepository = apiRepository;
    }

    public void loadComments(List<Integer> commentIds) {
        comments = apiRepository.loadComments(commentIds);
    }

    public void loadReplies(List<Integer> kids) {
        replies = apiRepository.loadChildComments(kids);
    }

    public LiveData<List<Comment>> getComments() {
        return comments;
    }

    public LiveData<List<Comment>> getReplies() {
        return replies;
    }

}
