package com.propertyguru.hackernews.ui.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.propertyguru.hackernews.api.ApiRepository;
import com.propertyguru.hackernews.api.model.Item;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by zr.faisal on 30/11/17.
 */

public class TopStoriesViewModel extends ViewModel {

    private ApiRepository apiRepository;
    private LiveData<List<Integer>> topStoryIds;
    private LiveData<List<Item>> topStories;

    @Inject
    public TopStoriesViewModel(ApiRepository apiRepository) {
        this.apiRepository = apiRepository;
    }

    public void loadTopStoryIds() {
        topStoryIds = apiRepository.loadTopStoryIds();
    }

    public void loadStories(List<Integer> stories) {
        topStories = apiRepository.loadTopStories(stories);
    }

    public LiveData<List<Integer>> getTopStoryIds() {
        return topStoryIds;
    }

    public LiveData<List<Item>> getTopStories() {
        return topStories;
    }

}
